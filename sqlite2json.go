package main

import(
_ "github.com/mattn/go-sqlite3"
	"time"
	"fmt"
	"database/sql"
	"log"
	"reflect"
	"encoding/json"
)

func queryToJson(db *sql.DB, query string, args ...interface{}) ([]byte, error) {
	// an array of JSON objects
	// the map key is the field name
	var objects []map[string]interface{}

	rows, err := db.Query(query, args...)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		// figure out what columns were returned
		// the column names will be the JSON object field keys
		columns, err := rows.ColumnTypes()
		if err != nil {
			return nil, err
		}

		// Scan needs an array of pointers to the values it is setting
		// This creates the object and sets the values correctly
		values := make([]interface{}, len(columns))
		object := map[string]interface{}{}
		for i, column := range columns {
			v := reflect.New(column.ScanType()).Interface()
			switch v.(type) {
			case *[]uint8:
				v = new(string)
			default:
				// use this to find the type for the field
				// you need to change
				// log.Printf("%v: %T", column.Name(), v)
			}

			object[column.Name()] = v
			values[i] = object[column.Name()]
		}

		err = rows.Scan(values...)
		if err != nil {
			return nil, err
		}

		objects = append(objects, object)
	}

	// indent because I want to read the output
	return json.MarshalIndent(objects, "", "\t")
}



func tm() string{
	db, err := sql.Open("sqlite3", "./1foo.db")
	if err != nil {
		log.Println(err)
	}
	defer db.Close()

	b, err := queryToJson(db, "select * from foo where foo1 = 1")
	if err != nil {
		fmt.Println(err)
	}

	return string(b)

}


func main() {
	start := time.Now()
	out := tm()
	t := time.Now()
	elapsed := t.Sub(start)
	fmt.Println(elapsed)
	fmt.Println(out)
}
