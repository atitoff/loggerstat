package main

import (
	"github.com/go-ini/ini"
	zmq "github.com/pebbe/zmq4"
	"fmt"
	"os"
	"time"
	"log"
	"github.com/vmihailenco/msgpack"
	"encoding/binary"
	"sync"
	"bytes"
	"io/ioutil"
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"encoding/json"
	"strings"
)

var version = "0.12"
var pubLoggerd, repBind, repLoggerd string

type CanMsg struct {
	Event     uint16
	DataBit   uint8
	Address   uint16
	Data      []byte
	Timestamp int64
}

type CanMsgEvent struct {
	Msg CanMsg
	Cnt uint64
}

type Heartbeat struct {
	Timestamp int64
	Data      []byte
}

type CanMsgPub struct {
	Timestamp int64
	Msg       []uint16
}

var db *sql.DB

var HeartBeatDict = make(map[uint32]Heartbeat)
var HeartBeatDictMutex = sync.RWMutex{}
var AddressDict = make(map[uint16][2]int64)
var AddressDictMutex = sync.RWMutex{}
var EventDict = make(map[uint16]CanMsgEvent)
var EventDictMutex = sync.RWMutex{}
var HeartBeatPeriod uint64

var cfg *ini.File

func receivePUB() {
	var unpackPubMsg CanMsgPub
	subscriber, _ := zmq.NewSocket(zmq.SUB)
	defer subscriber.Close()
	subscriber.Connect(pubLoggerd)
	subscriber.SetSubscribe("EV")
	time.Sleep(time.Second)

	for {
		msgPUB, e := subscriber.Recv(0)
		if e != nil {
			log.Println(e)
			break
		}
		e = msgpack.Unmarshal([]byte(msgPUB[7:]), &unpackPubMsg)

		// convert to CanMsg
		var dataCanMsg []byte
		for _, v := range unpackPubMsg.Msg[3:] {
			dataCanMsg = append(dataCanMsg, byte(v))
		}
		msg := CanMsg{
			Event:     unpackPubMsg.Msg[0],
			DataBit:   uint8(unpackPubMsg.Msg[1]),
			Address:   unpackPubMsg.Msg[2],
			Data:      dataCanMsg,
			Timestamp: unpackPubMsg.Timestamp,
		}

		// add to stat dict
		// Heartbeat
		if msg.Event == 45105 {
			moduleUID := binary.LittleEndian.Uint32(msg.Data)
			HeartBeatDictMutex.Lock()
			HeartBeatDict[moduleUID] = Heartbeat{msg.Timestamp, msg.Data[4:]}
			HeartBeatDictMutex.Unlock()
		}

		// AddressDict
		AddressDictMutex.Lock()
		arrAddress, _ := AddressDict[msg.Address]
		AddressDict[msg.Address] = [2]int64{arrAddress[0] + 1, msg.Timestamp}
		AddressDictMutex.Unlock()

		// EventDict
		if msg.Event > 0 && msg.Event <= 49151 {
			EventDictMutex.Lock()
			arrEvent, _ := EventDict[msg.Event]
			EventDict[msg.Event] = CanMsgEvent{msg, arrEvent.Cnt + 1}
			EventDictMutex.Unlock()
		}

	}
}

func RepServer() {
	var buf bytes.Buffer
	enc := msgpack.NewEncoder(&buf).StructAsArray(true)
	//  Socket to talk to clients
	responder, _ := zmq.NewSocket(zmq.REP)
	defer responder.Close()
	responder.Bind(repBind)

	for {
		//  Wait for next request from client
		msg, _ := responder.Recv(0)
		if msg[:1] == "\x00" { //**************  SQLite
			SQLite(msg[1:], responder)

		} else if msg[:1] == "\x04" { //*******  "last_address" сообщение
			AddressDictMutex.RLock()
			b, err := msgpack.Marshal(AddressDict)
			AddressDictMutex.RUnlock()
			if err != nil {
				panic(err)
			}
			responder.SendBytes(b, 0)
		} else if msg[:1] == "\x02" { //********  Set Heartbeat period
			var period uint64
			var err error
			err = msgpack.Unmarshal([]byte(msg[1:]), &period)
			if err != nil {
				responder.Send("\xC2", 0) // send false
			} else {
				if period < 0 {
					period = 0
				}
				if period > 8 {
					period = 8
				}
				log.Println("Set new heartbeat period: ", period)
				cfg.Section("").Key("heartbeat_period").SetValue(fmt.Sprintf("%d", period))
				err = cfg.SaveTo("./csh_loggerd.ini")
				if err != nil {
					responder.Send("\xC2", 0) // send false
				} else {
					responder.Send("\xC3", 0) // send true
				}
			}

		} else if msg[:1] == "\x03" { // Heartbeat report
			HeartBeatDictMutex.RLock()
			err := enc.Encode(HeartBeatDict)
			HeartBeatDictMutex.RUnlock()
			if err != nil {
				responder.Send("\xC0", 0)
			} else {
				readBuf, _ := ioutil.ReadAll(&buf)
				responder.SendBytes(readBuf, 0)
			}

		} else if msg[:1] == "\x01" { // last event
			var lastEvent uint16
			var err error
			err = msgpack.Unmarshal([]byte(msg[1:]), &lastEvent)
			if err != nil {
				responder.Send("\xC0", 0) // send nil
			} else {
				EventDictMutex.RLock()
				if lastEvent > 0 {
					err = enc.Encode(EventDict[lastEvent])
				} else {
					err = enc.Encode(EventDict)
				}
				EventDictMutex.RUnlock()

				if err != nil {
					responder.Send("\xC0", 0)
				} else {
					readBuf, _ := ioutil.ReadAll(&buf)
					responder.SendBytes(readBuf, 0)
				}
			}

		} else if msg[:1] == "\x10" { //********************* Systemd

		} else {
			responder.Send("\xC0", 0) // send nil
		}
	}

}

func ReqClient(canSendChan chan CanMsg) {

	worker, _ := zmq.NewSocket(zmq.REQ)
	defer worker.Close()
	worker.Connect(repLoggerd)

	for {
		msg := <-canSendChan // слушаем канал canSendChan

		var msgpackArr []uint16
		msgpackArr = append(msgpackArr, msg.Event)
		msgpackArr = append(msgpackArr, uint16(msg.DataBit))
		msgpackArr = append(msgpackArr, msg.Address)
		for _, v := range msg.Data {
			msgpackArr = append(msgpackArr, uint16(v))
		}
		b, _ := msgpack.Marshal(msgpackArr)
		worker.Send("\x00"+string(b[:]), 0)

	}
}

func heartbeat(canSendChan chan CanMsg) {
	var can CanMsg

	HeartBeatPeriod = cfg.Section("").Key("heartbeat_period").MustUint64()
	if HeartBeatPeriod < 0 {
		HeartBeatPeriod = 0
	}
	if HeartBeatPeriod > 8 {
		HeartBeatPeriod = 8
	}

	for {
		HeartBeatPeriodMs := 1 << (16 - HeartBeatPeriod) * 110          // plus 10%
		time.Sleep(time.Duration(HeartBeatPeriodMs) * time.Millisecond) // pause before
		can.Event = 45104
		can.DataBit = 0
		can.Address = 0
		can.Data = []byte{uint8(HeartBeatPeriod)}
		// отправляем в канал
		canSendChan <- can
	}

}

func getJSON(sqlString string, db *sql.DB) (string, error) {

	stmt, err := db.Prepare(sqlString)
	if err != nil {
		return "", err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		return "", err
	}
	defer rows.Close()

	columns, err := rows.Columns()
	if err != nil {
		return "", err
	}

	tableData := make([]map[string]interface{}, 0)

	count := len(columns)
	values := make([]interface{}, count)
	scanArgs := make([]interface{}, count)
	for i := range values {
		scanArgs[i] = &values[i]
	}

	for rows.Next() {
		err := rows.Scan(scanArgs...)
		if err != nil {
			return "", err
		}

		entry := make(map[string]interface{})
		for i, col := range columns {
			v := values[i]

			b, ok := v.([]byte)
			if ok {
				entry[col] = string(b)
			} else {
				entry[col] = v
			}
		}

		tableData = append(tableData, entry)
	}

	jsonData, err := json.Marshal(tableData)
	if err != nil {
		return "", err
	}

	return string(jsonData), nil
}

func SQLite(msg string, responder *zmq.Socket) {

	var err error
	var sqlRequest string

	if strings.Contains(strings.ToLower(msg), "select ") == false {
		errorStr := "SQLite request not begin `select`"
		resp, _ := json.Marshal(errorStr)
		responder.SendBytes(resp, 0)
		log.Println(msg, errorStr)
		return
	}

	err = json.Unmarshal([]byte(msg), &sqlRequest)
	if err != nil {
		errorStr := "error Unmarshal sql request"
		resp, _ := json.Marshal(errorStr)
		responder.SendBytes(resp, 0)
		log.Println(msg, errorStr)
		return
	}

	//outJson, err := queryToJson(db, sqlRequest)
	outJson, err := getJSON(sqlRequest, db)
	if err != nil {
		resp, _ := json.Marshal(err.Error())
		responder.SendBytes(resp, 0)
		log.Println(err)
		return
	}

	responder.Send(outJson, 0)

}

func main() {
	if len(os.Args) == 2 {
		if os.Args[1] == "-version" {
			fmt.Println(version)
			os.Exit(0)
		}
	}

	canSendChan := make(chan CanMsg)

	// load ini file
	var err error
	cfg, err = ini.Load("./csh_loggerstat.ini")
	if err != nil {
		log.Printf("Fail to read file: %v", err)
		os.Exit(1)
	}

	pubLoggerd = cfg.Section("").Key("pub_loggerd").MustString("tcp://localhost:5432")
	repBind = cfg.Section("").Key("rep_bind").MustString("tcp://*:5561")
	repLoggerd = cfg.Section("").Key("rep_loggerd").MustString("tcp://localhost:5560")

	db, err = sql.Open("sqlite3", "./foo.db")
	if err != nil {
		log.Println(err)
		return
	}
	defer db.Close()

	go receivePUB()

	go RepServer()

	go ReqClient(canSendChan)

	go heartbeat(canSendChan)

	for {
		time.Sleep(time.Second)
	}

}
